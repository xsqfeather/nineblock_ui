import Gun from "gun";
import { useEffect } from "react"

let gun:any = null;
export default function useGun(){
    useEffect(()=>{
        if(!gun){
            gun = Gun("http://localhost:8080/gun")
        }
    },[gun])
    return gun;
}