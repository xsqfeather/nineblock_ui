import { useState } from 'react';
import { gun } from '../gun';

interface InsertProps {
  resource: string;
}
export default function useInsert(props: InsertProps) {
  const { resource } = props;
  const [success, setSuccess] = useState(false);
  const insert = (data: any) => {
    const dataToInsert: string = JSON.stringify(data);
    gun.get(resource).put(
      {
        insert: dataToInsert,
      },
      (ack: any) => {
        console.log(ack);
        setSuccess(true);
      }
    );
  };
  return {
    insert,
    success,
  };
}
