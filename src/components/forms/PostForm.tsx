import { Button, Container, Paper, TextField, Typography } from '@mui/material';
import React, { useState } from 'react';
import { getGunTime } from '../../gun';
import useInsert from '../../hooks/useInsert';
import IpfsUploader from './IpfsUploader';
export default function PostForm(){
    const [title, setTitle ] = useState("")
    const [body, setBody ] = useState("")
    const { insert, success} = useInsert({resource: "posts"})

    const handleSubmit = (e:any) => {
        e.preventDefault();
        
        insert({
            title, body, createdAt: getGunTime(),
        });
    }   
    console.log({success});

    return (
        <Paper style={{
            paddingTop:10,
            paddingBottom: 10
        }}>
            <Container >
            <form onSubmit={handleSubmit} style={{
                display: 'flex',
                flexDirection: 'column',
            }}>
            <TextField onChange={(e:any) => { setTitle(e.target.value)}} label="title" />
            <br/>
            <div style={{display: 'flex', flexDirection: 'row', alignItems: "center", justifyContent: "left"}}>
                <Typography style={{marginRight: "5%", paddingLeft: 7}}>Upload Post Cover</Typography>
                <IpfsUploader />
            </div>
            <br/>

            <TextField multiline rows={10}  onChange={(e:any) => { setBody(e.target.value)}} label="body" />
            <br/>

            <Button fullWidth type="submit" variant="contained" color="primary">提交</Button>
            </form>
            </Container>
        </Paper>
    )
}