import React from 'react';
import {
  CircularProgress,
  Button,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent,
} from "@mui/material";
import { create } from "ipfs-http-client";
import { useEffect, useState } from "react";
import AddAPhotoIcon from "@mui/icons-material/Add";
import VisibilityIcon from "@mui/icons-material/Visibility";
import DeleteIcon from "@mui/icons-material/Delete";
let ipfsClient: any = null;

export type FileEventTarget = EventTarget & { files: FileList };

const IpfsUploader = (props: any) => {
  const [clientReady, setClientReady] = useState(false);
  const [image, setImage] = useState(props.image || "");
  const [uploading, setUploading] = useState(false);
  const [view, setView] = useState(false);

  const getIpfsClient = async function () {
    setClientReady(false);
    // connect to a different API
    ipfsClient = create({ url: "https://fileapi.woogege.com" });
    setClientReady(true);
    // call Core API methods
  };

  useEffect(() => {
    getIpfsClient();
  }, []);

  const handleChange = async (event: React.ChangeEvent<HTMLInputElement>): Promise<void> => {
    const files = event.target?.files;
    if(!files){
      return
    }
    setUploading(true);
    const file = files[0];
    if (!file) {
      setUploading(false);
      return;
    }
    const rlt = await ipfsClient.add(file);
    const url = "https://files.woogege.com/ipfs/" + rlt.path;
    setImage(url);
    setUploading(false);
    if (props.getUrl) {
      props.getUrl(url);
    }
    return;
  };

  const handleViewClick = async (event: any) => {
    event.stopPropagation();
    setView(true);
  };

  return (
    <>
      <Dialog
        open={view}
        onClose={() => {
          setView(false);
        }}
      >
        <DialogTitle>大图</DialogTitle>
        <DialogContent>
          <img
            alt={image}
            src={image}
            style={{
              maxWidth: "100%",
              height: "auto",
            }}
          />
        </DialogContent>
      </Dialog>
      {clientReady && (
        <Button
          style={{
            width: 150,
            height: 150,
            padding: 0,
            border: '1px dotted rgba(170, 50, 220, .6)',
            backgroundImage: `url(${image})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: "cover",
            backgroundPosition: "center"
          }}
          variant="text"
          component="label"

        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              width: "100%"
            }}
          >
            <div
              style={{
                textAlign: "center",
              }}
            >
              {uploading && <CircularProgress />}
              {image === ""
                ? !uploading && (
                    <AddAPhotoIcon fontSize="large" color="secondary" />
                  )
                : !uploading && (
                   <span>&nbsp;</span>
                  )}
              <input onChange={(e: React.ChangeEvent<HTMLInputElement>) => handleChange(e)} type="file" hidden />
            </div>
            {image !== "" && !uploading && (
              <div
                style={{
                  backgroundColor: "rgb(255 252 252 / 37%)",
                  position: "relative",
                  bottom: -42,
                  zIndex: 999,
                  textAlign: "center",
                }}
              >
                <IconButton onClick={handleViewClick} color="secondary">
                  <VisibilityIcon />
                </IconButton>
                <IconButton
                  onClick={(e: any) => {
                    e.stopPropagation();
                    e.preventDefault();
                    setImage("");
                    setUploading(false);
                  }}
                  color="secondary"
                >
                  <DeleteIcon />
                </IconButton>
              </div>
            )}
          </div>
        </Button>
      )}
    </>
  );

  
};


export default IpfsUploader;