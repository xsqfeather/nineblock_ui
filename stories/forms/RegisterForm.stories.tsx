import { Meta, Story } from '@storybook/react';
import React from 'react';
import RegisterForm from '../../src/components/forms/RegisterForm'

const meta: Meta = {
  title: 'Components/Forms/RegisterForm',
  component: RegisterForm,
  argTypes: {
    children: {
      control: {
        type: 'text',
      },
    },
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story<any> = args => <RegisterForm {...args} />;

// By passing using the Args format for exported stories, you can control the props for a component for reuse in a test
// https://storybook.js.org/docs/react/workflows/unit-testing
export const Default = Template.bind({});

Default.args = {};
