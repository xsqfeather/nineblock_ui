import React from 'react';
import { Meta, Story } from '@storybook/react';
import IpfsUploader from '../../src/components/forms/IpfsUploader';

const meta: Meta = {
  title: 'Components/Forms/IpfsUploader',
  component: IpfsUploader,
  argTypes: {
    children: {
      control: {
        type: 'text',
      },
    },
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story<any> = args => <IpfsUploader {...args} />;

export const Default = Template.bind({});

Default.args = {};
