import React from 'react';
import { Meta, Story } from '@storybook/react';
import NbCarousel from '../../src/components/data-view/NbCarousel';

const meta: Meta = {
  title: 'Components/DataView/NbCarousel',
  component: NbCarousel,
  argTypes: {
    children: {
      control: {
        type: 'text',
      },
    },
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story<any> = args => <NbCarousel {...args} />;

export const Default = Template.bind({});

Default.args = {};
