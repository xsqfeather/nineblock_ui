import { Meta } from '@storybook/react';
import React from 'react';
import BlogHomePage from "../src/pages/BlogHomePage"



export default {
  component: BlogHomePage,
  title: 'Pages/BlogHomePage',
} as Meta;
export const Primary: React.VFC<{}> = () => <BlogHomePage />;
export const ForOther: React.VFC<{}> = () => <BlogHomePage />;